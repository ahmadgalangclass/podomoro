import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { GlobalStyle } from './globalStyles';
import Hero from './components/Hero/Hero';
import Products from './components/Products/Product';
import { productData, productDataDua } from './components/Products/data';
import Feature from './components/Feature/Feature';
import Footer from './components/Footer/Footer';

function App() {
  return (
    <Router>
      <GlobalStyle />
      <Hero />
      <Products head="Pilih Selera Anda" data={productData} />
      <Feature />
      <Products head="Pilih Sambalnya" data={productDataDua} />
      <Footer />
    </Router>
  );
}

export default App;
