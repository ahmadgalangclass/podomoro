import React from 'react';
import {
  FooterContainer,
  FooterWrapper,
  SocialMediaContainer,
  SocialMediaWrapper,
  SocialLogo,
  SocialIcons,
  SocialIconLink,
} from './FooterElements';
import { FaFacebook, FaInstagram, FaTwitter } from 'react-icons/fa';

const Footer = () => {
  return (
    <FooterContainer>
      <FooterWrapper>
        <SocialMediaContainer>
          <SocialMediaWrapper>
            <SocialLogo to="/">PODO MORO</SocialLogo>
            <SocialIcons>
              <SocialIconLink href="/" targer="_blank" aria-label="Facebook">
                <FaFacebook />
              </SocialIconLink>
              <SocialIconLink href="/" targer="_blank" aria-label="Instagram">
                <FaInstagram />
              </SocialIconLink>
              <SocialIconLink href="/" targer="_blank" aria-label="Twitter">
                <FaTwitter />
              </SocialIconLink>
            </SocialIcons>
          </SocialMediaWrapper>
        </SocialMediaContainer>
      </FooterWrapper>
    </FooterContainer>
  );
};

export default Footer;
