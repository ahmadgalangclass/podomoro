export const productData = [
  {
    img:
      'https://asset-a.grid.id/crop/0x0:0x0/360x240/photo/sasefoto/original/43885_ayam-goreng-berempah.jpg',
    alt: 'goreng',
    name: 'Paha Goreng',
    desc: 'Daging Ayam goreng empuk dengan rasa khas',
    price: '10k',
    button: 'Tambahkan',
  },

  {
    img: 'https://kbu-cdn.com/dk/wp-content/uploads/ayam-panggang.jpg',
    alt: 'bakar',
    name: 'Paha Bakar',
    desc: 'Daging Ayam bakar dengan rasa gurih',
    price: '11k',
    button: 'Tambahkan',
  },

  {
    img:
      'https://awsimages.detik.net.id/community/media/visual/2016/07/15/ec4badd0-f951-4145-a2e0-3a214e66a1e1.jpg?a=1',
    alt: 'Crispy',
    name: 'Paha Crispy',
    desc: 'Daging Ayam crispy dengan tepung yang renyah',
    price: '11k',
    button: 'Tambahkan',
  },
];

export const productDataDua = [
  {
    img:
      'https://asset-a.grid.id/crop/0x0:0x0/360x240/photo/sasefoto/original/43885_ayam-goreng-berempah.jpg',
    alt: 'bawang',
    name: 'Sambal Bawang',
    desc: 'Rasa Sambal Bawang gurih',
    price: 'gratis',
    button: 'Tambahkan',
  },

  {
    img: 'https://kbu-cdn.com/dk/wp-content/uploads/sambal-kecap-boncabe.jpg',
    alt: 'kecap',
    name: 'Sambal Kecap',
    desc: 'Sambal Kecap pedas manis',
    price: 'gratis',
    button: 'Tambahkan',
  },

  {
    img:
      'https://kurio-img.kurioapps.com/20/04/17/f9eb103f-7893-4b0f-9c44-8471e62c93d2.jpeg',
    alt: 'trasi',
    name: 'Sambal Trasi',
    desc: 'Gabungan Trasi lezat',
    price: 'gratis',
    button: 'Tambahkan',
  },
];
