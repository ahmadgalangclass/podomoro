import React, { useState } from 'react';
import Navbar from '../Navbar';
import {
  HeroContainer,
  HeroContent,
  HeroItems,
  HeroHI,
  HeroP,
  HeroBtn,
} from './HeroElements';
import Sidebar from '../Sidebar/Sidebar';

const Hero = () => {
  const [isOpen, setIsOpen] = useState(false);

  const trigger = () => {
    setIsOpen(!isOpen);
  };

  return (
    <HeroContainer>
      <Navbar toggle={trigger} />
      <Sidebar isOpen={isOpen} toggle={trigger} />
      <HeroContent>
        <HeroItems>
          <HeroHI>Ayam Cita Rasa Terlezat</HeroHI>
          <HeroP>Penyajian Cepat, Beragam Sambal</HeroP>
          <HeroBtn>Pesan Segera</HeroBtn>
        </HeroItems>
      </HeroContent>
    </HeroContainer>
  );
};

export default Hero;
